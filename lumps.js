function switchAura(aura) {
  // If your first aura isn't Radiant Appetite then what's wrong with you?
  Game.dragonAura2 = aura;

  // Sacrifice one of the highest building because we're not heathens
  var highestBuilding = 0;
  for (var i in Game.Objects) { if (Game.Objects[i].amount>0) highestBuilding = Game.Objects[i];}
  if (highestBuilding != 0) Game.ObjectsById[highestBuilding.id].sacrifice(1);
}

setInterval(function() {
  // If there is less than a minute until the lump is ripe
  if ((Game.lumpT + Game.lumpRipeAge) - Date.now() < 60000) {
    // If the lump is normal
    if (Game.lumpCurrentType == 0) {
      // Reroll the RNG
      Game.seed = Game.makeSeed();
      Game.computeLumpType();
    }
  } else {
    // Try for a golden or meaty lump
    if (Game.lumpCurrentType < 2) {
      Game.seed = Game.makeSeed();
      Game.computeLumpType();
    }
  }

  // Lump is ripe
if ((Date.now()-Game.lumpT) > Game.lumpRipeAge) {
  // Golden lump
  if (Game.lumpCurrentType == 2) {
    var currentAura = Game.dragonAura2;
    var itemsOwned = [];

    // Switch to Earth Shatterer (if not already set) - see function below
    if (currentAura != 5) switchAura(5);

    // Get all current building amounts
    for (var i in Game.Objects) {
      var thing = Game.Objects[i];
      itemsOwned.push({name:thing.name,amount:thing.amount});
      thing.sell(thing.amount);
    }

    Game.clickLump();

    // Switch back to current aura while we have no buildings
    switchAura(currentAura);

    // Buy back all buildings
    for (var i in itemsOwned) {
      Game.Objects[itemsOwned[i].name].buy(itemsOwned[i].amount);
    }
    // Buy back an extra Chancemaker to replace the one sacrificed when switching auras (if required)
    if (currentAura !=5) Game.Objects["Chancemaker"].buy(1);
  } else {
    // Lump wasn't golden so we just simply harvest
    Game.clickLump();
  }
}
},500);